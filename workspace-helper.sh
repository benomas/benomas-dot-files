#!/bin/bash
function fixWorkspaces()
{
    i3-msg "workspace databases-files, move workspace to output HDMI-A-0"
    i3-msg "workspace terminals, move workspace to output DisplayPort-4"
    i3-msg "workspace code, move workspace to output DisplayPort-0"
    i3-msg "workspace personal, move workspace to output DisplayPort-1"
    i3-msg "workspace internet, move workspace to output DisplayPort-5"
}

function autoloadQpc()
{
    if [ "$HOSTNAME" = qpc ] || [ "$HOSTNAME" = benomas-min ] || [ "$HOSTNAME" = benomas-home-pc ] || [ "$HOSTNAME" = "benomas-home-pc" ];
        then
            gnome-terminal && gnome-terminal && gnome-terminal && gnome-terminal && gnome-terminal && gnome-terminal && gnome-terminal && gnome-terminal && code && (nohup firefox-dev & disown) && (nohup firefox-dev & disown) && (nohup dbeaver & disown) && (nohup mysql-workbench-community & disown) && (nohup google-chrome & disown) && (nohup google-chrome & disown) && phpstorm;
        else
            echo "no compatible pc";
	fi
}

function desktopGrid()
{
    i3-msg "workspace code; append_layout ~/.config/i3/workspace-code.json"
    i3-msg "workspace personal; append_layout ~/.config/i3/workspace-personal.json"
    i3-msg "workspace internet; append_layout ~/.config/i3/workspace-internet.json"
    i3-msg "workspace databases-files; append_layout ~/.config/i3/workspace-databases-files.json"
    i3-msg "workspace terminals; append_layout ~/.config/i3/workspace-terminals.json"
    autoloadQpc
}
