#!/bin/sh
xrandr --output DisplayPort-1 --primary --mode 2560x1440 --pos 0x1080 --rotate inverted --output DisplayPort-2 --mode 3840x2160 --pos 2560x1080 --rotate normal --output DisplayPort-3 --off --output HDMI-A-1 --mode 1920x1080 --pos 2619x0 --rotate inverted --output DisplayPort-2-0 --off --output HDMI-2-0 --off --output DVI-2-0 --off
