#!/bin/bash
function cvInstallCurl()
{
    yes | sudo apt install curl
}

function cvInstallFirefox()
{
    cd ~/Downloads
    sudo wget "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US" -O Firefox-dev.tar.bz2
    sudo sudo tar xjf  Firefox-dev.tar.bz2 -C /opt/
    cd ~/Downloads
    sudo rm Firefox-dev.tar.bz2
    sudo ln -s /opt/firefox/firefox /usr/local/bin/firefox-dev
}

function cvInstallPhpStorm()
{
    cd ~/Downloads
    sudo wget "https://download.jetbrains.com/idea/ideaIU-2023.2.1.tar.gz" -O phpstorm.tar.gz
    sudo sudo tar -xzvf phpstorm.tar.gz -C ~/app-data/instalations
    cd ~/Downloads
    sudo rm phpstorm.tar.gz
    sudo ln -s ~/app-data/instalations/idea-IU-232.9559.62/bin/idea.sh /usr/local/bin/storm
}

function cvInstallDocker()
{
    yes | sudo apt install docker.io docker-compose
}

function cvInstallNode()
{
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    yes | sudo apt install -y nodejs
}

function cvInstallNode14()
{
    curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    yes | sudo apt install -y nodejs
}

function cvInstallNpm()
{
    yes | sudo apt install npm
}

function cvInstallFzf()
{
    yes | sudo apt install fzf
}

function cvInstallWoeUsb()
{
    yes | sudo add-apt-repository ppa:nilarimogard/webupd8
    yes | sudo apt update
    yes | sudo apt install woeusb
}

function cvInstallI3()
{
    #first download and uncompres i3 files
    yes | sudo apt build-dep i3-wm
    ./configure
    #run build command, suggested by configure script
    cd /home/benomas/Downloads/i3-4.18.1/x86_64-pc-linux-gnu && make -j8
    yes | make && sudo make install
    yes | sudo apt install i3status dmenu i3lock xbacklight feh conky
}

function cvInstallDbeaver()
{
  sudo snap install dbeaver-ce
}

function cvFixSass(){
    npm rebuild node-sass
}

function cvInstallTranslate()
{
    yes | sudo apt install translate-shell
}

function cvInstallWps()
{
    yes | wget http://kdl.cc.ksosoft.com/wps-community/download/6757/wps-office_10.1.0.6757_amd64.deb
    yes | sudo dpkg -i wps-office_*.deb
    yes | wget http://ftp.debian.org/debian/pool/main/libp/libpng/libpng12-0_1.2.50-2+deb8u3_amd64.de
    yes | sudo dpkg -i libpng12-0_1.2.50-2+deb8u3_amd64.deb
    yes | sudo apt -f install
}

function cvFixDependences()
{
    sudo apt --fix-broken install
}

function cvInstallYarn()
{
    sudo apt remove cmdtest
    sudo apt remove yarn
    sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
    sudo npm install -g yarn
}

function cvInstallGitKraken()
{
    yes | sudo apt install gconf2
    yes | sudo apt --fix-broken install
    cd ~/Downloads
    yes | wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
    fins gitkraken-amd64.deb
}

function cvInstallExtras()
{
    yes | sudo apt install ubuntu-restricted-extras
    yes | sudo apt install libavcodec-extra
    yes | sudo apt install libdvd-pkg
}

function cvInstallTelegram()
{
    yes | sudo snap install telegram-desktop
}

function cvInstallScrot()
{
    yes | sudo apt install scrot
}

function cvInstallAc1200(){
    yes | git clone https://github.com/cilynx/rtl88x2bu
    cd rtl88x2bu
    VER=$(sed -n 's/\PACKAGE_VERSION="\(.*\)"/\1/p' dkms.conf)
    yes | sudo rsync -rvhP ./ /usr/src/rtl88x2bu-${VER}
    yes | sudo dkms add -m rtl88x2bu -v ${VER}
    yes | sudo dkms build -m rtl88x2bu -v ${VER}
    yes | sudo dkms install -m rtl88x2bu -v ${VER}
    yes | sudo modprobe 88x2bu
}

function cvInstallAc120058(){
    #for linux kernel 5.8
    yes | sudo git clone "https://github.com/RinCat/RTL88x2BU-Linux-Driver.git" /usr/src/rtl88x2bu-git
    yes | sudo sed -i 's/PACKAGE_VERSION="@PKGVER@"/PACKAGE_VERSION="git"/g' /usr/src/rtl88x2bu-git/dkms.conf
    yes | sudo dkms add -m rtl88x2bu -v git
    yes | sudo dkms dkms autoinstall
    yes | sudo modprobe 88x2bu rtw_switch_usb_mode=1
    #restart required
}

function cvInstallInsomina(){
    # Add to sources
    echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" \
        | sudo tee -a /etc/apt/sources.list.d/insomnia.list

    # Add public key used to verify code signature
    yes | wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc \
        | sudo apt-key add -

    # Refresh repository sources and install Insomnia
    yes | sudo apt update
    yes | sudo apt install insomnia
}

function cvInstallFileZilla(){
  sudo apt install filezilla
}

function cvInstallOhMyZsh(){
    yes | sudo apt install zsh
    yes | sudo sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
}

function cvInstallObsStudio(){
    yes | sudo apt-get install ffmpeg
    yes | sudo add-apt-repository ppa:obsproject/obs-studio
    yes | sudo apt-get update && sudo apt-get install obs-studio
}

function cvInstallMsr(){
    echo msr | sudo tee -a /etc/modules
}

function cvRunZenStates () {
    yes | sudo python3 ~/apps/ZenStates-Linux/zenstates.py --no-gui --disable --c6-disable
}

function cvRunGuiZenStates () {
    yes | sudo python3 ~/apps/ZenStates-Linux/zenstates.py
}

function cvInstallPhp74 () {
    yes | sudo apt install php7.4-cli
}

function cvPurgePhp7 () {
    yes | sudo apt-get purge php7.*
}

function howToMountADisk(){
    echo "
        2.1 Create a mount point
            sudo mkdir /hddgn
        2.1 Create a mount point
            sudo nano /etc/fstab
        And add following to the end of the file:
            /dev/sdb1    /hdd    ext4    defaults    0    0
        2.3 Mount partition
            sudo mount /hdd

        reference:https://askubuntu.com/questions/125257/how-do-i-add-an-additional-hard-drive
    "
}

function howToCopyFoldersWithOriginalPermissions(){
    echo "
        sudo rsync -a source target
        sudo rsync -a .laradock-infys/ app-data/
    "
}

function cvChangeMysqlRootPass () {
    mysql -u root -p
    ALTER USER 'root'@'localhost' IDENTIFIED BY 'nueva_contraseña';
    FLUSH PRIVILEGES;
    exit;
}

function cvDropMysqlUser () {
    mysql -u root -p
    DROP USER 'nombre_usuario'@'host';
    DROP USER 'nombre_usuario';
    FLUSH PRIVILEGES;
    exit;
}

function cvInstallPostman()
{
    cd ~/Downloads
    sudo wget "https://dl.pstmn.io/download/latest/linux_64" -O postman.tar.gz
    sudo tar xzvf  postman.tar.gz -C /opt/
    cd ~/Downloads
    sudo rm postman.tar.gz
    if [ -d "/opt/Postman" ]; then
        # Borra la carpeta si existe
        rm -r "$carpeta_a_verificar"
        echo "La carpeta ha sido eliminada."
    else
        echo "La carpeta no existe."
    fi

   sudo rm /usr/local/bin/postman && sudo ln -s /home/benomas/Downloads/postman-linux-x64/Postman/Postman /usr/local/bin/postman
    sudo rm /usr/local/bin/postman && sudo ln -s /home/benomas/Downloads/postman-linux-x64/Postman/Postman /usr/local/bin/postman
}
