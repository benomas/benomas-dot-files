sudo wget "https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh" -O install.sh
sudo chmod +x install.sh
cd ~/apps/<app-folder>
nvm install 12
nvm alias default 12
npm install
npm install -g @quasar/cli@1.4.0
