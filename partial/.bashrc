# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
    xterm*|rxvt*)
        PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
    *)
    ;;
esac
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
        elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 "[[%s]]")\$ '
HISTFILESIZE=5000;
HISTSIZE=5000;

alias cd..='cd ..'

alias generaldockerstop='sudo docker-compose stop'
alias generaldockerdown='sudo docker-compose down'
alias generaldockerquit='generaldockerstop && generaldockerdown'
alias generaldockerup='sudo docker-compose up -d'
alias generaldockerrerebuildfpm="sudo docker-compose build --no-cache php-fpm"
alias generaldockerrebuildworkspace="sudo docker-compose build --no-cache workspace"
alias generaldockerrebuildnginx="sudo docker-compose build --no-cache nginx"
alias generaldockerrebuildphpmyadmin="sudo docker-compose build --no-cache phpmyadmin"
alias generaldockerrebuildmssql="sudo docker-compose build --no-cache mssql"
alias generaldockerbash='sudo docker-compose exec --user=laradock workspace bash'
alias generaldockerphpfpm='sudo docker-compose exec --user=laradock php-fpm bash'
alias generaldockermssql='sudo docker-compose exec --user=laradock mssql bash'
alias generaldockermysql='sudo docker-compose exec --user=laradock mysql bash'
alias generaldockermariadb='sudo docker-compose exec --user=laradock mariadb bash'
alias generaldockerrbash='sudo docker-compose exec --user=root workspace bash'
alias generaldockerrphpfpm='sudo docker-compose exec --user=root php-fpm bash'
alias generaldockerrmssql='sudo docker-compose exec --user=root mssql bash'
alias generaldockerrmysql='sudo docker-compose exec --user=root mysql bash'
alias generaldockerrmariadb='sudo docker-compose exec --user=root mariadb bash'
alias generaldockerscript='sudo docker-compose exec workspace'

declare -A laradockV82
laradockV82[prefix]='php82-laradock'
laradockV82[project]='php82-laradock'
laradockV82[path]='~/apps/laradocks/php82-laradock'
laradockV82[datapath]='~/laradocks/.php82-laradock/data'

declare -a laradockConfigs=("laradockV82" "laradockV" "infysdockV" )

for val in ${laradockConfigs[@]}; do
    declare -n laradockEnvV="$val"
    alias cd${laradockEnvV[prefix]}="cd ${laradockEnvV[path]}"

    alias ${laradockEnvV[prefix]}rebuildmariadb="sudo rm -rf ${laradockEnvV[datapath]}/mariadb && sudo docker-compose build --no-cache mariadb"
    alias ${laradockEnvV[prefix]}rebuildmysql="sudo rm -rf ${laradockEnvV[datapath]}/mysql && sudo docker-compose build --no-cache mysql"

    alias ${laradockEnvV[prefix]}stop="cd${laradockEnvV[prefix]} && generaldockerstop"
    alias ${laradockEnvV[prefix]}down="cd${laradockEnvV[prefix]} && generaldockerdown"
    alias ${laradockEnvV[prefix]}quit="cd${laradockEnvV[prefix]} && generaldockerquit"
    alias ${laradockEnvV[prefix]}up="cd${laradockEnvV[prefix]} && generaldockerup"
    alias ${laradockEnvV[prefix]}rebuildfpm="cd${laradockEnvV[prefix]} && generaldockerrerebuildfpm"
    alias ${laradockEnvV[prefix]}rebuildworkspace="cd${laradockEnvV[prefix]} && generaldockerrebuildworkspace"
    alias ${laradockEnvV[prefix]}rebuildnginx="cd${laradockEnvV[prefix]} && generaldockerrebuildnginx"
    alias ${laradockEnvV[prefix]}rebuildmyadmin="cd${laradockEnvV[prefix]} && generaldockerrebuildphpmyadmin"
    alias ${laradockEnvV[prefix]}rebuildmssql="cd${laradockEnvV[prefix]} && generaldockerrebuildmssql"
    alias ${laradockEnvV[prefix]}bash="cd${laradockEnvV[prefix]} && generaldockerbash"
    alias ${laradockEnvV[prefix]}fpmbash="cd${laradockEnvV[prefix]} && generaldockerphpfpm"
    alias ${laradockEnvV[prefix]}mssqlbash="cd${laradockEnvV[prefix]} && generaldockermssql"
    alias ${laradockEnvV[prefix]}mysqlbash="cd${laradockEnvV[prefix]} && generaldockermysql"
    alias ${laradockEnvV[prefix]}mariadbbash="cd${laradockEnvV[prefix]} && generaldockermariadb"
    alias ${laradockEnvV[prefix]}rbash="cd${laradockEnvV[prefix]} && generaldockerrbash"
    alias ${laradockEnvV[prefix]}rfpmbash="cd${laradockEnvV[prefix]} && generaldockerrphpfpm"
    alias ${laradockEnvV[prefix]}rmssqlbash="cd${laradockEnvV[prefix]} && generaldockerrmssql"
    alias ${laradockEnvV[prefix]}rmysqlbash="cd${laradockEnvV[prefix]} && generaldockerrmysql"
    alias ${laradockEnvV[prefix]}rmariadbbash="cd${laradockEnvV[prefix]} && generaldockerrmariadb"
    alias ${laradockEnvV[prefix]}script="cd${laradockEnvV[prefix]} && generaldockerscript"
    alias ${laradockEnvV[prefix]}fpm=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mysql adminer phpmyadmin && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
    alias ${laradockEnvV[prefix]}pmaria=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mariadb adminer phpmyadmin && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
    alias ${laradockEnvV[prefix]}allin=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mysql mariadb postgres adminer phpmyadmin && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
done

alias git-fastpush='git add -u && git commit -m "No relevant message" && git push'
alias nanobash="nano ~/.bashrc"
alias rebash="source ~/.bashrc && clear"
alias nanohosts="sudo nano /etc/hosts"
#i3 layout commands
alias openc='code $(fzf -m)'
alias clearlogs='if [ -f storage/logs/laravel.log ]; then rm -R storage/logs/*.log; echo "Found laravel.log"; else echo "laravel.log not found."; fi'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
alias fgp="git add . && git commit -m \"add last changes with fast commit\" && git push"
alias dscrs='scrot -c -d $1'
alias get-class='xprop | grep "CLASS"'
alias cleanLogs='sudo truncate -s 0 /var/log/syslog && sudo truncate -s 0 /var/log/kern.log'
alias monitorsOn='xset s off && xset -dpms && xset s noblank'
alias sshcache='eval $(ssh-agent) && ssh-add'
alias check-permissions="ls -l"
alias add-to-group="sudo usermod -a -G sudo"