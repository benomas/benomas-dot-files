#!/bin/bash
function fixKeyLayout()
{
    setxkbmap -layout latam
}

function currentKeyLayout()
{
    setxkbmap -query | grep layout
}

function fixKeyLayoutUs()
{
    setxkbmap -layout us -variant altgr-intl
}
