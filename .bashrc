# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
    xterm*|rxvt*)
        PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
    *)
    ;;
esac
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
        elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 "[[%s]]")\$ '
HISTFILESIZE=5000;
HISTSIZE=5000;

if [ -f ~/workspace-helper.sh ]; then
    . ~/workspace-helper.sh
    #workspaces helpers
    alias fix-workspaces='fixWorkspaces'
    alias desktop-grid='desktopGrid'
    alias autoload-qpc='autoloadQpc'
fi
if [ -f ~/reinstalls.sh ]; then
    . ~/reinstalls.sh
fi
if [ -f ~/fixes.sh ]; then
    . ~/fixes.sh
    fixKeyLayoutUs
fi
alias cd..='cd ..'

alias generaldockerstop='docker-compose stop'
alias generaldockerdown='docker-compose down'
alias generaldockerquit='generaldockerstop && generaldockerdown'
alias generaldockerup='docker-compose up -d'
alias generaldockerrerebuild="docker-compose build --no-cache"
alias generaldockerrerebuildfpm="docker-compose build --no-cache php-fpm"
alias generaldockerrebuildworkspace="docker-compose build --no-cache workspace"
alias generaldockerrebuildnginx="docker-compose build --no-cache nginx"
alias generaldockerrebuildphpmyadmin="docker-compose build --no-cache phpmyadmin"
alias generaldockerrebuildmssql="docker-compose build --no-cache mssql"
alias generaldockerbash='docker-compose exec --user=laradock workspace bash'
alias generaldockerphpfpm='docker-compose exec --user=laradock php-fpm bash'
alias generaldockermssql='docker-compose exec --user=laradock mssql bash'
alias generaldockermysql='docker-compose exec --user=laradock mysql bash'
alias generaldockermariadb='docker-compose exec --user=laradock mariadb bash'
alias generaldockerrbash='docker-compose exec --user=root workspace bash'
alias generaldockerrphpfpm='docker-compose exec --user=root php-fpm bash'
alias generaldockerrmssql='docker-compose exec --user=root mssql bash'
alias generaldockerrmysql='docker-compose exec --user=root mysql bash'
alias generaldockerrmariadb='docker-compose exec --user=root mariadb bash'
alias generaldockerscript='docker-compose exec workspace'
alias generaldockerbashadminer='docker-compose exec adminer bash'

declare -A laradockV82
laradockV82[prefix]='php82-laradock'
laradockV82[project]='php82-laradock'
laradockV82[path]='~/apps/laradocks/php82-laradock'
laradockV82[datapath]='~/laradocks/.php82-laradock/data'

declare -A laradockV81
laradockV81[prefix]='php81-laradock'
laradockV81[project]='php81-laradock'
laradockV81[path]='~/apps/laradocks/php81-laradock'
laradockV81[datapath]='~/laradocks/.php81-laradock/data'

declare -A laradockV80
laradockV80[prefix]='php80-laradock'
laradockV80[project]='php80-laradock'
laradockV80[path]='~/apps/laradocks/php80-laradock'
laradockV80[datapath]='~/laradocks/.php80-laradock/data'

declare -A laradockV74
laradockV74[prefix]='php74-laradock'
laradockV74[project]='php74-laradock'
laradockV74[path]='~/apps/laradocks/php74-laradock'
laradockV74[datapath]='~/laradocks/.php74-laradock/data'

declare -A laradockV
laradockV[prefix]='laradock'
laradockV[project]='laradock'
laradockV[path]='~/apps/laradock'
laradockV[datapath]='~/.laradock/.data'

declare -A infysdockV
infysdockV[prefix]='infysdock'
infysdockV[project]='laradock'
infysdockV[path]='~/app-data/apps/infysdock'
infysdockV[datapath]='~/.laradock-infys/.data'

declare -a laradockConfigs=("laradockV82" "laradockV81" "laradockV80" "laradockV74" "laradockV" "infysdockV" )

for val in ${laradockConfigs[@]}; do
    declare -n laradockEnvV="$val"
    alias cd${laradockEnvV[prefix]}="cd ${laradockEnvV[path]}"

    alias ${laradockEnvV[prefix]}rebuildmariadb="sudo docker-compose build --no-cache mariadb"
    alias ${laradockEnvV[prefix]}rebuildmysql="sudo docker-compose build --no-cache mysql"

    alias ${laradockEnvV[prefix]}stop="cd${laradockEnvV[prefix]} && generaldockerstop"
    alias ${laradockEnvV[prefix]}down="cd${laradockEnvV[prefix]} && generaldockerdown"
    alias ${laradockEnvV[prefix]}quit="cd${laradockEnvV[prefix]} && generaldockerquit"
    alias ${laradockEnvV[prefix]}up="cd${laradockEnvV[prefix]} && generaldockerup"
    alias ${laradockEnvV[prefix]}rebuild="cd${laradockEnvV[prefix]} && generaldockerrerebuild"
    alias ${laradockEnvV[prefix]}rebuildfpm="cd${laradockEnvV[prefix]} && generaldockerrerebuildfpm"
    alias ${laradockEnvV[prefix]}rebuildworkspace="cd${laradockEnvV[prefix]} && generaldockerrebuildworkspace"
    alias ${laradockEnvV[prefix]}rebuildnginx="cd${laradockEnvV[prefix]} && generaldockerrebuildnginx"
    alias ${laradockEnvV[prefix]}rebuildmyadmin="cd${laradockEnvV[prefix]} && generaldockerrebuildphpmyadmin"
    alias ${laradockEnvV[prefix]}rebuildmssql="cd${laradockEnvV[prefix]} && generaldockerrebuildmssql"
    alias ${laradockEnvV[prefix]}bash="cd${laradockEnvV[prefix]} && generaldockerbash"
    alias ${laradockEnvV[prefix]}fpmbash="cd${laradockEnvV[prefix]} && generaldockerphpfpm"
    alias ${laradockEnvV[prefix]}mssqlbash="cd${laradockEnvV[prefix]} && generaldockermssql"
    alias ${laradockEnvV[prefix]}mysqlbash="cd${laradockEnvV[prefix]} && generaldockermysql"
    alias ${laradockEnvV[prefix]}mariadbbash="cd${laradockEnvV[prefix]} && generaldockermariadb"
    alias ${laradockEnvV[prefix]}rbash="cd${laradockEnvV[prefix]} && generaldockerrbash"
    alias ${laradockEnvV[prefix]}rfpmbash="cd${laradockEnvV[prefix]} && generaldockerrphpfpm"
    alias ${laradockEnvV[prefix]}rmssqlbash="cd${laradockEnvV[prefix]} && generaldockerrmssql"
    alias ${laradockEnvV[prefix]}rmysqlbash="cd${laradockEnvV[prefix]} && generaldockerrmysql"
    alias ${laradockEnvV[prefix]}rmariadbbash="cd${laradockEnvV[prefix]} && generaldockerrmariadb"
    alias ${laradockEnvV[prefix]}bashadminer="cd${laradockEnvV[prefix]} && generaldockerbashadminer"
    alias ${laradockEnvV[prefix]}script="cd${laradockEnvV[prefix]} && generaldockerscript"
    alias ${laradockEnvV[prefix]}fpm=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mysql adminer phpmyadmin && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
    alias ${laradockEnvV[prefix]}pmaria=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mariadb adminer phpmyadmin && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
    alias ${laradockEnvV[prefix]}allin=$"cd${laradockEnvV[prefix]} && generaldockerup nginx mysql mariadb postgres mongo adminer phpmyadmin php-worker && ${laradockEnvV[prefix]}bash -c 'cd /var/www && exec \"/bin/bash\"'"
done

alias postgressdockstop='cd ~/apps/tomcat-env && sudo docker-compose stop'
alias postgressdockdown='cd ~/apps/tomcat-env && sudo docker-compose down'
alias postgressdockquit='cd ~/apps/laradock && postgressdockstop && postgressdockdown'
alias postgressdockup='cd ~/apps/tomcat-env && sudo docker-compose up -d'

alias cv-cp-bolsa='rsync -avu --delete /home/benomas/apps/bolsa-de-trabajo/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-cime33='rsync -avu --delete /home/benomas/apps/cime33/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-infys='rsync -avu --delete /home/benomas/app-data/apps/infys-universal/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-phibro='rsync -avu --delete /home/benomas/apps/phibro-fatten-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-solar='rsync -avu --delete /home/benomas/apps/solar-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-volter='rsync -avu --delete /home/benomas/apps/volter-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-pro-subastas='rsync -avu --delete /home/benomas/apps/pro-subastas-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-elearning='rsync -avu --delete /home/benomas/apps/elearning-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-wimo='rsync -avu --delete /home/benomas/apps/wimo-services-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-josema='rsync -avu --delete /home/benomas/apps/josema-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-demo='rsync -avu --delete /home/benomas/apps/demo-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-backbone='rsync -avu --delete /home/benomas/apps/backbone/backbone-prodemo-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-ff-conferences='rsync -avu --delete /home/benomas/apps/ff-conferences/api-ff-conferences/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-troubleshooting='rsync -avu --delete /home/benomas/apps/telmovpay-troubleshooting-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-cmbackend='rsync -avu --delete /home/benomas/apps/cm-backend/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cv-cp-woofi='rsync -avu --delete /home/benomas/apps/woofi-api/vendor/benomas/crudvel/src /home/benomas/apps/crudvel'
alias cvu-cp-bolsa='rsync -avu --delete /home/benomas/apps/bolsa-de-trabajo-app/node_modules/crudvuel-tools/src /home/benomas/apps/crudvuel-tools/'
alias cvu-cp-infys='rsync -avu --delete /home/benomas/app-data/apps/infys-web-app/node_modules/crudvuel-tools/src /home/benomas/apps/crudvuel-tools/'
alias cvu-cp-phibro='rsync -avu --delete /home/benomas/apps/phibro-fatten-web-app/node_modules/crudvuel-tools/src /home/benomas/apps/crudvuel-tools/'
alias cvu-cp-solar='rsync -avu --delete /home/benomas/apps/solar-web-app/node_modules/crudvuel-tools/src /home/benomas/apps/crudvuel-tools/'
alias cvu-cp-volter='rsync -avu --delete /home/benomas/apps/volter-web-app/node_modules/crudvuel-tools/src /home/benomas/apps/crudvuel-tools/'
alias cvu-cp-pro-subastas='rsync -avu --delete "/home/benomas/apps/pro-subastas-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-elearning='rsync -avu --delete "/home/benomas/apps/elearning-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-wimo='rsync -avu --delete "/home/benomas/apps/wimo-services-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-josema='rsync -avu --delete "/home/benomas/apps/josema-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-demo='rsync -avu --delete "/home/benomas/apps/demo-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cwimo-cp='rsync -avu --delete "/home/benomas/apps/wimo3/custom-wimo-assets/css" "/home/benomas/apps/custom-wimo-assets" && rsync -avu --delete "/home/benomas/apps/wimo3/custom-wimo-assets/html" "/home/benomas/apps/custom-wimo-assets" && rsync -avu --delete "/home/benomas/apps/wimo3/custom-wimo-assets/js" "/home/benomas/apps/custom-wimo-assets" && rsync -avu --delete "/home/benomas/apps/wimo3/custom-wimo-assets/sql" "/home/benomas/apps/custom-wimo-assets" && rsync -avu --delete "/home/benomas/apps/wimo3/custom-wimo-assets/img" "/home/benomas/apps/custom-wimo-assets"'
alias cwimobridge-cp='rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/proxy" "/home/benomas/apps/cv-api-bridge" && rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/public" "/home/benomas/apps/cv-api-bridge" && rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/vendor" "/home/benomas/apps/cv-api-bridge" && rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/composer.json" "/home/benomas/apps/cv-api-bridge" && rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/composer.lock" "/home/benomas/apps/cv-api-bridge" && rsync -avu --delete "/home/benomas/apps/wimo3/cv-api-bridge/composer.lock" "/home/benomas/apps/cv-api-bridge"'
#alias w-cp-wimoreports='rsync -avu --delete /home/benomas/apps/wimoreportes/node_modules/w-libraries/projects /home/benomas/apps/w-libraries/'
alias w-cp-wimoreports='rsync -avu --delete /home/benomas/apps/wimoreportes/node_modules/wi-angular-base-classes/src /home/benomas/apps/w-libraries/projects/wi-angular-base-classes'
alias w-cp-wimochild='rsync -avu --delete /home/benomas/apps/wimo3/wp-content/themes/wimoChild /home/benomas/apps/wimo-child/src'
alias cvu-cp-ff-conferences='rsync -avu --delete "/home/benomas/apps/ff-conferences/web-app-ff-conferences/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-troubleshooting='rsync -avu --delete "/home/benomas/apps/telmovpay-troubleshooting-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'
alias cvu-cp-woofi='rsync -avu --delete "/home/benomas/apps/woofi-web-app/node_modules/crudvuel-tools/src" "/home/benomas/apps/crudvuel-tools/"'

alias backup-dotfiles='~/apps/dotfiles/backup.sh'
alias install-dotfiles='~/apps/dotfiles/install.sh'
alias git-fastpush='git add -u && git commit -m "No relevant message" && git push'
alias gdotfiles='cd ~/apps/dotfiles'
alias fast-df-push='backup-dotfiles && gdotfiles && git-fastpush'
alias nanobash="nano ~/.bashrc"
alias atombash="atom ~/.bashrc"
alias codebash="code ~/.bashrc"
alias rebash="source ~/.bashrc && clear"
alias ginfysu="cd ~/app-data/apps/infys-universal"
alias ginfysw="cd ~/app-data/apps/infys-web-app"
alias shutdown='poweroff'
alias restart='reboot'
alias nanohosts="sudo nano /etc/hosts"
alias codehosts="sudo code /etc/hosts"
alias ssuspend='sudo pm-suspend'
#i3 layout commands
alias openc='code $(fzf -m)'
alias clearlogs='if [ -f storage/logs/laravel.log ]; then rm -R storage/logs/*.log; echo "Found laravel.log"; else echo "laravel.log not found."; fi'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
alias openr='~/app-data/apps/infys-universal/openr'
alias i3exit='i3-msg exit'
alias i3reload='i3-msg reload'
alias fgp="git add . && git commit -m \"add last changes with fast commit\" && git push"
alias controlscaff="cd $HOME/app-data/apps/infys-universal/pscaff/controlScripts && ./control.py"
alias dscrs='scrot -c -d $1'
alias scrs='SF_NAME="%Y-%m-%d_$wx$h.png";scrot $SF_NAME -e "mv $SF_NAME ~/Pictures/"'
alias sscrs='SF_NAME="%Y-%m-%d-%s__$wx$h.png";scrot -s $SF_NAME -e "mv $SF_NAME ~/Pictures/"'
alias fins='sudo dpkg -i'
alias get-class='xprop | grep "CLASS"'
alias esen="trans es:en"
alias enes="trans en:es"
alias reloaddots='yes | install-dotfiles -y && rebash'
alias preloaddots='gdotfiles && fgp && yes | install-dotfiles -y && rebash'
alias cleanLogs='sudo truncate -s 0 /var/log/syslog && sudo truncate -s 0 /var/log/kern.log'
ANDROID_HOME="$HOME/Android/Sdk";
#JAVA_HOME="/usr/lib/jdk1.8.0_212";
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64";
alias mainssh="ssh 192.168.100.196"
alias minssh="ssh 192.168.100.213"
alias min2ssh="ssh 192.168.100.223"
alias monitorsOn='xset s off && xset -dpms && xset s noblank'
alias sshcache='eval $(ssh-agent) && ssh-add'
alias disableMidle="xbindkeys"
alias disableCamera="sudo modprobe -r uvcvideo"
alias start-tomcat="sudo systemctl start tomcat"
alias stop-tomcat="sudo systemctl stop tomcat"
alias status-tomcat="sudo systemctl status tomcat"
alias check-permissions="ls -l"
alias add-to-group="sudo usermod -a -G sudo"
alias callRazerCommand="cd ~/apps/razercommander/builddir && ninja run"
alias androidSt="cd ~/android-studio/bin && ./studio.sh"
alias regrid="cd ~/.screenlayout && ./config1.sh && fixWorkspaces"
alias infysbackup="cd ~/app-data/apps/infys-universal/pscaff/controlScripts && sudo ./infys-drive.py --backup"
alias infysrestore="cd ~/app-data/apps/infys-universal/pscaff/controlScripts && sudo ./infys-drive.py --backup"
alias setDefaultNode="nvm alias default"
alias ubuntuVersion="lsb_release -a"
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
alias localTenantAutostart=""
alias pwoofissh="ssh pwoofi"
alias cvAutoClean="sudo rm -rf /tmp/* && sudo apt-get clean && sudo apt-get autoclean && rm -rf ~/.cache/thumbnails/* && sudo journalctl --vacuum-size=50M && rm -rf ~/.cache/* && rm -rf ~/.local/share/Trash/*"
alias cvRefresh='sudo apt update && sudo apt upgrade'

export ANDROID_HOME;
export JAVA_HOME;
if [ -d $ANDROID_HOME ]; then
    PATH=$PATH:$ANDROID_HOME/tools;
    PATH=$PATH:$ANDROID_HOME/platform-tools;
fi
if [ -d $JAVA_HOME ]; then
    PATH=$PATH:$JAVA_HOME;
    PATH=$PATH:$JAVA_HOME/bin;
fi

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
