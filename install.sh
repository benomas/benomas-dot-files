#!/bin/bash

# Script to install dotfiles
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Config git
#catch script path
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"'/'
function git_config(){
	echo "[i] Configuring git";
	#set git global ignore
	cat << EOF > ~/.gitignore
*~
*.un~
EOF
git config --global core.excludesfile '~/.gitignore'
git -v >& /dev/null
if [ $? -ne 0 ]; then
	#Git is installed :)
	#echo "Type your git user.name"
	#read name
	#Set git config
	#git config --global user.name $name
	git config --global user.name benomas
	#echo "Type your git user.email"
	#read email
	#git config --global user.email $email
	git config --global user.email benomas@gmail.com
	#Set default editor vi
	git config --global core.editor vi
fi
}

function bashrc(){
	echo "Copying .bashrc";
	cp $SCRIPTPATH'.'bashrc ~/
	cp $SCRIPTPATH''workspace-helper.sh ~/
	cp $SCRIPTPATH''reinstalls.sh ~/
	cp $SCRIPTPATH''fixes.sh ~/
	cp $SCRIPTPATH''other_scripts.sh ~/
}

function i3_config(){
	echo "Copying .config/i3";
	cp -R $SCRIPTPATH'/'i3/ ~/.config/
}

function i3_grid(){
	echo "Copying .screenlayout";
	cp -R $SCRIPTPATH'/.screenlayout' ~/
}

function main(){
	echo "[i] Installing dotfiles";

	#bashrc
	echo -e "[?] Copy .bashrc (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bashrc
	fi

	#i3
	echo -e "[?] Copy .config/i3 (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		i3_config
	fi

	#i3
	echo -e "[?] Copy .screenlayout (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		i3_grid
	fi

	#Git
	#echo -e "\n[?] Set git config (y/n)";
	#read -n1 option
	#if [ "$option" == "y" ]; then
	#	git_config
	#fi

	echo "";
}

#Call to main function
main
