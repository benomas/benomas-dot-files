#!/bin/bash

# Script to backup dotfiles
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#bashrc
#catch script path
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"'/'
function bk_bashrc(){
	echo "[i] Copying .bashrc";
	cp ~/.bashrc $SCRIPTPATH
	cp ~/workspace-helper.sh $SCRIPTPATH
	cp ~/reinstalls.sh $SCRIPTPATH
	cp ~/fixes.sh $SCRIPTPATH
	cp ~/other_scripts.sh $SCRIPTPATH
}

#i3 config
function bk_i3_config(){
	echo "[i] Copying .i3 configs";
	cp  -R ~/.config/i3/ $SCRIPTPATH/
}

#i3 grid
function bk_i3_grid(){
	echo "[i] Copying .i3 grid";
	cp  -R ~/.screenlayout/ $SCRIPTPATH/
}

#Main function
function main(){
	echo "[i] Backup dotfiles";

	echo -e "[?] Backup ~/.bashrc (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_bashrc
	fi

	echo -e "[?] Backup ~/.config/i3 (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_i3_config
	fi

	echo -e "[?] Backup ~/.screenlayout (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_i3_grid
	fi
}

#Call main function
main
#Call git
git -v >& /dev/null
if [ $? -ne 0 ]; then
	#Git is installed
	#Call git diff
	#echo -e "[?] Show git diff (y/n)";
	#read -n1 option
	#if [ "$option" == "y" ]; then
	#	git diff
	#fi

fi

echo -e "\nScript ends";
